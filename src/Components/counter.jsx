import React, { Component } from 'react';
class Counter extends Component
{
   ////state={
    //    count:this.props.counter.value
        // to set image path dynamically imageUrl : "path"
        //<img src="{this.state.imageUrl}" alt=''/>
   // };

   // constructor() binding Event Handlers
    //{
     //   super();
     //   this.handleIncrement = this.handleIncrement.bind(this);
    //}     OR

    
    //handleIncrement = (product)  =>
    //{
    ///    console.log(product);
    //    this.setState({ count : this.state.count + 1});
    //};

    render()
    {
        return (
                    <div> 
                        <span  className={this.getBadgeClasses()} 
                        style={{fontSize: '20px',padding:'2px',
                        margin:'5px' }}>{ this.formatCount()} 
                         </span>
                       
                         <button onClick={() => 
                         this.props.onIncrement(this.props.counter) }
                         className="btn btn-secondary btn-sm"
                         >Increment</button>

                         <button onClick={() =>
                             this.props.onDelete(this.props.counter.id)} 
                             className="btn btn-danger btn-sm m-2">
                                 Delete</button>
                    </div>
                );
    }

    getBadgeClasses() {
        let classes = "badge m-4 badge-";
        classes += (this.props.counter.count === 0) ? "warning" : "primary";
        return classes;
    }

    formatCount()
    {
        const {count}=this.props.counter;
        
        return count=== 0 ? 'zero' : count;
    }
}

export default Counter;